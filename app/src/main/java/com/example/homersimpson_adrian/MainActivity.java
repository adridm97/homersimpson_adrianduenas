package com.example.homersimpson_adrian;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    MediaPlayer media;

    ImageView donut;
        ImageView engranajeRojo;
        ImageView engranajeVerde;
        ImageView engranajeAzul;
        ImageView ojo;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        donut = findViewById(R.id.imgDonut);
        donut.setVisibility(View.INVISIBLE);
        ojo = findViewById(R.id.imgOjo);
        ojo.setVisibility(View.INVISIBLE);
        engranajeRojo = findViewById(R.id.imgEngranajeRojo);
        engranajeRojo.setVisibility(View.INVISIBLE);
        engranajeVerde = findViewById(R.id.engranajeVerde);
        engranajeVerde.setVisibility(View.INVISIBLE);
        engranajeAzul = findViewById(R.id.engranajeAzul);
        engranajeAzul.setVisibility(View.INVISIBLE);

        ImageView titulo = findViewById(R.id.imgTitol);
        titulo.setOnClickListener(new View.OnClickListener(){
         public void onClick(View v){
             Animation animar_rojo = AnimationUtils.loadAnimation(v.getContext(), R.anim.animar_rojo);
             Animation animar_azul = AnimationUtils.loadAnimation(v.getContext(), R.anim.animar_azul);
             Animation animar_verde = AnimationUtils.loadAnimation(v.getContext(), R.anim.animar_verde);
             Animation animar_donut = AnimationUtils.loadAnimation(v.getContext(), R.anim.animar_donut);
             Animation animar_ojo = AnimationUtils.loadAnimation(v.getContext(), R.anim.animar_ojo);
             if (donut.getVisibility()==View.INVISIBLE){
                 donut.setVisibility(View.VISIBLE);
                 donut.startAnimation(animar_donut);
                 ojo.setVisibility(View.VISIBLE);
                 ojo.startAnimation(animar_ojo);
                 engranajeRojo.setVisibility(View.VISIBLE);
                 engranajeRojo.startAnimation(animar_rojo);
                 engranajeVerde.setVisibility(View.VISIBLE);
                 engranajeVerde.startAnimation(animar_verde);
                 engranajeAzul.setVisibility(View.VISIBLE);
                 engranajeAzul.startAnimation(animar_azul);

             }else{
                 donut.setVisibility(View.INVISIBLE);
                 ojo.setVisibility(View.INVISIBLE);
                 engranajeRojo.setVisibility(View.INVISIBLE);
                 engranajeVerde.setVisibility(View.INVISIBLE);
                 engranajeAzul.setVisibility(View.INVISIBLE);
                 donut.clearAnimation();
                 ojo.clearAnimation();
                 engranajeRojo.clearAnimation();
                 engranajeAzul.clearAnimation();
                 engranajeVerde.clearAnimation();
             }



         }
        });
        ImageView donut = findViewById(R.id.imgDonut);
        donut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                    if (media != null && media.isPlaying())
                    {
                        media.stop();
                    }
                    else
                    {
                        media = MediaPlayer.create(getApplicationContext(), R.raw.the_simpsons);
                        media.start();
                    }

            }
        });

    }

}
